import React, {Component} from "react";
import {Provider, connect} from "react-redux";
import {createStore, combineReducers} from "redux";
import Webcam from "react-webcam";
import "./App.css";
import Awesome from "../public/awesome.gif";

//--------------------------------------------------------------------------------------------------------------------//
//---CONSTANTS--------------------------------------------------------------------------------------------------------//
//--------------------------------------------------------------------------------------------------------------------//

//Define los posibles estados en los que puede estar la aplicación
const states = {
    'TAKING_BEAUTY': 'TAKING_BEAUTY',
    'WAITING_FOR_VOTE': 'WAITING_FOR_VOTE',
    'VOTING': 'VOTING',
    'FINISHED_VOTING': 'FINISHED_VOTING'
};

const imageWidth = 320;
const imageHeight = 240;

//--------------------------------------------------------------------------------------------------------------------//
//---ACTION-GENERATORS------------------------------------------------------------------------------------------------//
//--------------------------------------------------------------------------------------------------------------------//
//Son los encargados de proveer el 'template' de las posibles acciones a dispatchear

//Agrega una imagen al array de imagenes
const addImage = image => {
    return {
        type: 'ADD_IMAGE',
        image: image
    }
};

//Sobreescribe el array de imagenes
const setImages = images => {
    return {
        type: 'SET_IMAGES',
        images: images
    }
};

//Sobreescribe el estado de la aplicación. Si es estado no es válido se setea en 'UNDEFINED_STATE'
const changeAppState = newState => {
    return {
        type: 'CHANGE_APP_STATE',
        state: states[newState] || 'UNDEFINED_STATE'
    }
};

//Sobreescribe la imagen ganadora
const setWinner = image => {
    return {
        type: 'SET_WINNER',
        image: image
    }
};

//--------------------------------------------------------------------------------------------------------------------//
//---HELPER-FUNCTIONS-------------------------------------------------------------------------------------------------//
//--------------------------------------------------------------------------------------------------------------------//

//Se utiliza para remover una imagen aleatoriamente del array de imagenes
const removeRandomImage = images => {
    const index = Math.floor((Math.random() * images.length));
    return images.filter((v, i) => i !== index);
};

//Se utiliza para manejar el proceso de votación. Dispatchea varios métodos
const vote = (images, dispatch) => {
    dispatch(changeAppState('VOTING'));
    //Elimina de forma aleatoria una de las imagenes cada 1000ms hasta que queda la ganadora
    const interval = setInterval(()=> {
        images = removeRandomImage(images);
        if (images.length !== 1) {
            dispatch(setImages(images));
        } else {
            clearInterval(interval);
            dispatch(setWinner(images[0]));
            dispatch(setImages([]));
            dispatch(changeAppState('FINISHED_VOTING'));
        }
    }, 1000);
};

//--------------------------------------------------------------------------------------------------------------------//
//---PRESENTATIONAL-COMPONENTS----------------------------------------------------------------------------------------//
//--------------------------------------------------------------------------------------------------------------------//

//Botón simple que acepta como parámetros una acción a realizar y un label
const Button = ({onClick, children = 'Label'}) => <button className="btn btn-default"
                                                          onClick={onClick}>{children}</button>;

//Imagen que acepta como parámetro el origen de la misma
const Image = ({source, height, width}) => <img src={source} height={height} width={width} alt="screenshot"/>;

//Lista de Images
const ImageList = ({images}) => {
    return (
        <div className="row">
            {
                images.map(
                    (image, index) =>
                        <div className="col-lg-4" key={index}><Image source={image}/></div>
                )
            }
        </div>
    )
};

//Setea los parámetros de configuración de la Webcam y permite sacar fotos
//Si bien es una clase, es solo para poder usar 'this'
class WebcamWrapper extends Component {
    constructor(props) {
        super(props);

        //Necesario para poder utilizar 'this' dentro de la función screenshot
        this.screenshot = this.screenshot.bind(this);
    }

    screenshot() {
        //this.refs.webcam.getScreenshot() es el método mediante el cual se obtiene una nueva imagen de la webcam
        this.props.takeImage(this.refs.webcam.getScreenshot());
    };

    render() {
        return (
            <div className="text-center">
                <Webcam width={imageWidth} height={imageHeight} screenshotFormat="image/png" ref="webcam"/>
                <div>
                    <Button onClick={this.screenshot}>Take Image</Button>
                </div>
            </div>
        )

    }
}

//Estructura de la aplicación
const App = () => {
    return (
        <div className="container">
            <div className="row text-center">
                <h1>Self Beautines Contest</h1>
                <div className="col-lg-4">
                    <div className="panel panel-default">
                        <div className="panel-body">
                            <WebCamContainer />
                            <ButtonsContainer />
                        </div>
                    </div>
                </div>
                <div className="col-lg-8">
                    <WinnerContainer />
                    <ImageListContainer />
                </div>
            </div>
        </div>
    )
};

//--------------------------------------------------------------------------------------------------------------------//
//---COMPLEX-COMPONENTS-----------------------------------------------------------------------------------------------//
//--------------------------------------------------------------------------------------------------------------------//

//Gestiona la aparición de la webcam y controla el número de imagenes a tomar antes de empezar la votación
class WebCam extends Component {
    constructor(props) {
        super(props);
        this.addImage = this.addImage.bind(this);
    }

    addImage(image) {
        const props = this.props;
        props.addImage(image);
        props.images.length == 5 ? props.finishedTakingBeauty() : undefined;
    };

    render() {
        return (
            <div>
                {
                    this.props.state === 'TAKING_BEAUTY' &&
                    <WebcamWrapper takeImage={this.addImage}/>
                }
            </div>
        )
    }
}

//Gestiona la aparición de los botones y permite dispatchear las funciones 'startVotation' y 'restartVotation'
const Buttons = ({state, images, startVotation, restartVotation}) => {
    return (
        <div>
            {
                state === 'WAITING_FOR_VOTE' &&
                (
                    <div>
                        <Button onClick={() => startVotation(images)}>Start Voting</Button>
                    </div>
                )
            }
            {
                state === 'VOTING' &&
                (
                    <div><h4>Voting...</h4></div>
                )
            }
            {
                state === 'FINISHED_VOTING' &&
                (
                    <div>
                        <h4>And the winner is</h4>
                        <Button onClick={restartVotation}>Restart Votation</Button>
                    </div>
                )
            }
        </div>
    )
};

//Gestiona la aparición de la imagen ganadora
const Winner = ({image}) => {
    return (
        <div>
            {
                image &&
                <div>
                    <div><Image source={image}/></div>
                    <div><Image source={Awesome} height={imageHeight} width={imageWidth}/></div>
                </div>
            }
        </div>
    )
};

//--------------------------------------------------------------------------------------------------------------------//
//---CONTAINERS-------------------------------------------------------------------------------------------------------//
//--------------------------------------------------------------------------------------------------------------------//
//Gestionan la unión del state de redux con los componentes y sus propiedades

const mapStateToImageListProps = state => {
    return {
        images: state.images
    }
};
const ImageListContainer = connect(mapStateToImageListProps)(ImageList);

const mapStateToWebCamProps = state => {
    return {
        state: state.state,
        images: state.images
    }
};
const mapDispatchToWebCamProps = (dispatch, ownProps) => {
    //porque no lo inyecta?
    //console.log(ownProps);
    return {
        addImage: image => dispatch(addImage(image)),
        finishedTakingBeauty: () => dispatch(changeAppState('WAITING_FOR_VOTE'))
    }
};
const WebCamContainer = connect(mapStateToWebCamProps, mapDispatchToWebCamProps)(WebCam);

const mapStateToButtonsProps = state => {
    return {
        state: state.state,
        images: state.images
    }
};
const mapDispatchToButtonsProps = dispatch => {
    return {
        startVotation: images => vote(images, dispatch),
        restartVotation: () => {
            dispatch(setWinner(null));
            dispatch(changeAppState('TAKING_BEAUTY'));
        }
    }
};
const ButtonsContainer = connect(mapStateToButtonsProps, mapDispatchToButtonsProps)(Buttons);

const mapStateToWinnerProps = state => {
    return {
        image: state.image
    }
};
const WinnerContainer = connect(mapStateToWinnerProps)(Winner);

//--------------------------------------------------------------------------------------------------------------------//
//---REDUCERS---------------------------------------------------------------------------------------------------------//
//--------------------------------------------------------------------------------------------------------------------//

//Reducer que maneja la variable 'images' del state.
const images = (state = [], action) => {
    switch (action.type) {
        case 'ADD_IMAGE':
            return [...state, action.image];
        case 'SET_IMAGES':
            return action.images;
        default:
            return state;
    }
};

//Reducer que maneja la variable 'image' del state.
const image = (state = null, action) => {
    switch (action.type) {
        case 'SET_WINNER':
            return action.image;
        default:
            return state;
    }
};

//Reducer que maneja la variable 'state' del state.
const state = (state = 'TAKING_BEAUTY', action) => {
    switch (action.type) {
        case 'CHANGE_APP_STATE':
            return action.state;
        default:
            return state;
    }
};

//--------------------------------------------------------------------------------------------------------------------//
//---STORE-CREATION---------------------------------------------------------------------------------------------------//
//--------------------------------------------------------------------------------------------------------------------//
//Crea el store principal en base a la combinación de los reducers 'image', 'images' y 'state'
const store = createStore(combineReducers({image, images, state}));

//--------------------------------------------------------------------------------------------------------------------//
//---EXPORTS----------------------------------------------------------------------------------------------------------//
//--------------------------------------------------------------------------------------------------------------------//
//Exporta la aplicación para ser renderizada

//El provider ser encarga de inyectar el estado en los children a través del context
export default () => (
    <Provider store={store}>
        <App />
    </Provider>
);
