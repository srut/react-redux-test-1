import React from 'react';
import ReactDOM from 'react-dom';
import Application from './App';
import './index.css';

//Función que renderiza el default export de App en el elemento cuyo id es 'root'
ReactDOM.render(<Application />, document.getElementById('root'));
